# Comptes twitter de service d'Archives

## Les Archives départementales

- 21 services ont un compte twitter.
- 4 ne sont pas présents dans l’annuaire du SIAF
- L’Alsace a remplacé le Bas-Rhin mais ce n’est pas à jour sur France archives
- La Sarthe et l’Ain déclarent des comptes twitter sur France Archives A mais en fait ce sont les comptes des Conseils départementaux
- Pourtant l'Ain a bien un compte propre
- Le nombre médian d’abonnés est de 1326
- Le plus petit compte a 10 abonnés (Eure) et le plus important 3667 (Les Yvelines)
- 5 comptes sont inactifs, dont un depuis 10 ans et avec seulement 1 mois d’activité (la Somme)
- Parmi les 14 comptes actifs, le plus ancien est de 2011 (L’Aube) et le plus récent de août 2022 (Eure)

## Les Archives municipales

- 22 services ont un compte twitter
- 2 ne sont pas présents dans l’annuaire du SIAF 
- Le nombre médian d’abonnés est de 1178 
- Le plus petit compte a 117 abonné (Angoulême)) et le plus important 11511 (Lyon) 
- 5 comptes sont inactifs depuis au moins 5 ans, dont un n’a aucune publication (Angoulême) 
- Parmi les 17 comptes actifs, les plus anciens sont de juin 2012 (Brest et Bordeaux) et le plus récent de juin 2021 (Centre de Gestion du Nord)

## Ressources associées :
- [Page dédiée](https://francearchives.fr/article/37775) à la présence sur les réseaux sociaux des services d'Archives par France archives
- [Liste des archives départementales sur Twitter](https://framagit.org/Mac_Graveur/ad_twitter/-/blob/main/Twitter_AD_20220818.csv). Recensement au 18/08/2022. CSV en UTF-8 et point-virugule en séparateur.
- Liste ouverte des comptes d'archives départementales sur [Twitter](https://twitter.com/i/lists/1448183149360451584)
- [Liste des archives municipales sur Twitter](https://framagit.org/Mac_Graveur/ad_twitter/-/blob/main/Twitter_AM_20220818.csv)
- Liste ouverte des comptes d'archives municipales sur [Twitter](https://twitter.com/i/lists/1448218476959739908)
